# 3DMove_automation

Automation of the graphical user interface (GUI) for 3D-move analysis software (http://www.arc.unr.edu/Software.html) via Python.
The automation was needed because of the lack of a batch running mode for 3D-Move Anlysis software.
We suggest to run the code on a remote machine because while running 3D-Move needs to be the active window.

Even if under development we hope that you can find the code useful! We would like you to reference it if you use it.

Arpad & Giulia 