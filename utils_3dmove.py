
# Before starting:
# - create an input file called "master_template.3dv2" and place it in your BASE_DIR
# - create in your BASE_DIR two folders: "ongoing_analysis" and "completed_analyses"
# - create two folders in RUN_DIR: "Temp" and "OutputFiles"

# Notes:
# - the input parameter which is now changed is the stiffness of the second asphalt layer. Adjust it to your needs.
# - the maximum horizontal strain of the second layer is now collected. Adjust it to your needs

import pandas as pd
import time
import os
import glob

from pywinauto.application import Application
from pywinauto.keyboard import send_keys
from shutil import copy

BASE_DIR = "C:\\Users\\giuli\\pavement_probabilistic\\Analysis\\"  # Insert here your base directory
RUN_DIR = os.path.join(BASE_DIR, "ongoing_analysis")
RES_DIR = os.path.join(BASE_DIR, "completed_analyses")
OUT_DIR = os.path.join(RUN_DIR, "OutputFiles")

# --------------------------------------------------------------------------
# UTILITY FUNCTIONS (to be moved to a separate py file)
# --------------------------------------------------------------------------


def clear_folder(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)


def wait_for_file(file_path):
    flag = False
    while not flag:
        if os.path.isfile(file_path):
            # print("the file was added!")
            time.sleep(10)
            flag = True
        else:
            time.sleep(5)
    return flag


def run_3dmove(X, ID=None):
    # --------------------------------------------------------------------------
    # PRE-PROCESS
    # --------------------------------------------------------------------------
    # remove previous input file
    file_path = os.path.join(RUN_DIR, "ongoing_analysis.3dv2")
    if os.path.isfile(file_path):
        os.unlink(file_path)

    # remove previous analysis files
    folder_path = os.path.join(RUN_DIR, "OutputFiles")
    clear_folder(folder_path)
    folder_path = os.path.join(RUN_DIR, "Temp")
    clear_folder(folder_path)

    # copy the new input file to the analysis folder
    src = os.path.join(BASE_DIR, "master_template.3dv2")
    dst = os.path.join(RUN_DIR, "ongoing_analysis.3dv2")
    copy(src, dst)

    # change your relevant shit in ongoing_analysis
    base = os.path.splitext(dst)[0]

    with open(dst, 'r') as file:
    # read a list of lines into data
        data = file.readlines()


    loc = list()
    for (elem_loc, elem) in enumerate(data):
        if "'!!!! Materials No" in elem:
            loc = elem_loc
            print(loc)
            #loc.append(elem_loc)
            #loc = loc[0]
            if '2\n' in data[loc+1]:
                print(data[loc+6])
                data[loc+6] = str(round(X, 2)) + '\n'

    # save the new file
    with open(RUN_DIR + '\\' + 'ongoing_analysis.3dv2', 'w') as file:
        file.writelines(data)


    # --------------------------------------------------------------------------
    # RUN
    # --------------------------------------------------------------------------
    # run 3D-MOVE
    app = Application(backend="win32").connect(process=13724
                                               , title_re=".*Move.*")
    # You need to update process number evrytime you open 3D move, you can check the process ID from the task manager
    time.sleep(5)
    send_keys("%FO")
    time.sleep(2)
    send_keys(os.path.join(RUN_DIR, "ongoing_analysis.3dv2"))
    time.sleep(5)
    send_keys("{ENTER}")  # open the file
    time.sleep(5)
    send_keys("{ENTER}")  # kill the warning message
    time.sleep(1)

    # press the run button
    # this is needed as it can change how many tabs are needed to find the Run button and after the Run has started
    # we want to avoid cancelling it
    for jj in range(10): 
        send_keys(f"{{TAB {jj}}}")  # go to the Analysis button
        send_keys("{ENTER}")  # Run the analysis finally... or not
        time.sleep(3)
        if os.path.isfile(os.path.join(RUN_DIR, "Temp", "ongoing_analysis.3DM")):
            break

    start = time.time()
    print(f'Analysis with X1={X} is running.')
    wait_for_file(os.path.join(RUN_DIR, "OutputFiles", "ongoing_analysis_Acc.txt"))
    end = time.time()

    # close the file
    send_keys("{ENTER}")  # closing possible Team viewer message (useful only if you are running in remote)
    send_keys("%FC")
    time.sleep(1)
    send_keys("{ENTER}")

    print(f'The analysis with X={X} has been completed. (net analysis time= {round(end - start)} sec)')

    # --------------------------------------------------------------------------
    # POST-PROCESS
    # --------------------------------------------------------------------------
    # get your data
    f = open(OUT_DIR + '\\' + "ongoing_analysis_NStrain.txt", 'r')
    # read a list of lines into data
    data = f.readlines()

    # cleaning and adjusting the data
    del data[0:35]
    k = []
    for i in data:
        j = i.replace(' .', '0.')
        j = j.replace(' -.', '-0.')
        j = j.replace('      ', ';')
        j = j.replace('     ', ';')
        j = j.replace('   ', '')
        j = j.replace('\n', '')
        j = j.replace('\n', '')
        j = j.replace('E', 'e')
        k.append(j)

    # transform into a df
    df = pd.DataFrame(k, columns=["TIME"])

    # results of the first point in 3D-move
    df = df.iloc[0:1024]
    df[['TIME', 'EXX', 'EYY', 'EZZ']] = df['TIME'].str.split(';', expand=True)
    df = df.apply(pd.to_numeric)
    strain_xx = df['EXX'].min() * 10 ** (-6)

    # create folder for results
    dest_dir = os.path.join(RES_DIR, f"analysis_{ID}")
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)

    # copy files
    src_dir = os.path.join(RUN_DIR, "OutputFiles", "*.txt")
    for file in glob.glob(src_dir):
        copy(file, dest_dir)

    return strain_xx